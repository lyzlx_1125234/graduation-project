var websocket = null;
if ('WebSocket' in window) {
  websocket = new WebSocket('ws://127.0.0.1:8080/webSocket')
} else {
  console.log('该浏览器不支持webSocket！')
}
websocket.onopen = function (event) {
  console.log('建立连接！')
}
websocket.onclose = function (event) {
  console.log('关闭连接！')
}
websocket.onmessage = function get(event) {
  console.log(JSON.parse(event.data))
  return JSON.parse(event.data)
}
websocket.onerror = function () {
  console.log('webSocket通信发生错误')
}
websocket.onbeforeunload = function () {
  websocket.close();
}