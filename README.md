# 毕业设计

#### 介绍
毕业设计"基于Echarts全国疫情大屏展示与设计"，源码包括前端代码，后端代码和数据库脚本

#### 软件架构

1.yiqing-project为后端代码
2.yiqing-vision为前端代码
3.lykmysql.sql为数据库脚本

#### 安装教程

1.后端项目编程语言为Java，框架采用SpringBoot，Mybatis，Maven，数据库为MySQL5.7
2.前端项目使用的是vue框架，Echarts可视化

#### 使用说明

1.本项目后端首先导入数据库脚本，IDEA配置数据库账号密码，配置maven并更新
2.前端通过npm install安装所需的包
3.该项目的数据库脚本并不是最新数据，所有数据都来源于百度疫情官方发布
4.需要最新脚本私信
5.参考网页http://lykcomeon.top