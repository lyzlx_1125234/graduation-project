package com.study.enums;

/**
 * @Author: liyuankun
 * @Date: 2021/2/18 16:49
 * @Description:  区域URL枚举类
 **/
public enum AreaUrlEnum {


    beijing("北京", "https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E5%8C%97%E4%BA%AC&stage=publish&callback=jsonp_1613638272423_36177"),
    heilongjiang("黑龙江", "https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E9%BB%91%E9%BE%99%E6%B1%9F&stage=publish&callback=jsonp_1613638319330_94279"),
    jilin("吉林", "https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E5%90%89%E6%9E%97&stage=publish&callback=jsonp_1613638341238_2984"),
    liaoning("辽宁","https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E8%BE%BD%E5%AE%81&stage=publish&callback=jsonp_1613638374372_6933"),
    neimenggu("内蒙古","https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E5%86%85%E8%92%99%E5%8F%A4&stage=publish&callback=jsonp_1613638430546_54051"),
    hebei("河北","https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E6%B2%B3%E5%8C%97&stage=publish&callback=jsonp_1613638495753_45299"),
    tianjin("天津","https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E5%A4%A9%E6%B4%A5&stage=publish&callback=jsonp_1613638531341_63562"),
    shandong("山东","https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E5%B1%B1%E4%B8%9C&stage=publish&callback=jsonp_1613638566640_11396"),
    shanxi("山西","https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E5%B1%B1%E8%A5%BF&stage=publish&callback=jsonp_1613638641497_65427"),
    sanxi("陕西","https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E9%99%95%E8%A5%BF&stage=publish&callback=jsonp_1613638669599_76409"),
    ningxia("宁夏","https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E5%AE%81%E5%A4%8F&stage=publish&callback=jsonp_1613638712632_21559"),
    gansu("甘肃","https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E7%94%98%E8%82%83&stage=publish&callback=jsonp_1613638750748_29075"),
    qinghai("青海","https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E9%9D%92%E6%B5%B7&stage=publish&callback=jsonp_1613638799540_55498"),
    xinjiang("新疆","https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E6%96%B0%E7%96%86&stage=publish&callback=jsonp_1613640353523_37880"),
    xizang("西藏","https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E8%A5%BF%E8%97%8F&stage=publish&callback=jsonp_1613640381674_6426"),
    jiangsu("江苏","https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E6%B1%9F%E8%8B%8F&stage=publish&callback=jsonp_1613638854207_40759"),
    anhui("安徽","https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E5%AE%89%E5%BE%BD&stage=publish&callback=jsonp_1613638898207_88626"),
    henan("河南","https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E6%B2%B3%E5%8D%97&stage=publish&callback=jsonp_1613638915158_51874"),
    hubei("湖北","https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E6%B9%96%E5%8C%97&stage=publish&callback=jsonp_1613638967596_69142"),
    chongqing("重庆","https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E9%87%8D%E5%BA%86&stage=publish&callback=jsonp_1613639002214_91994"),
    sichuan("四川","https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E5%9B%9B%E5%B7%9D&stage=publish&callback=jsonp_1613639017021_54807"),
    shanghai("上海","https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E4%B8%8A%E6%B5%B7&stage=publish&callback=jsonp_1613639056889_90483"),
    zhejiang("浙江","https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E6%B5%99%E6%B1%9F&stage=publish&callback=jsonp_1613639073961_2110"),
    jiangxi("江西","https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E6%B1%9F%E8%A5%BF&stage=publish&callback=jsonp_1613639097734_82143"),
    hunan("湖南","https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E6%B9%96%E5%8D%97&stage=publish&callback=jsonp_1613639123708_90910"),
    guizhou("贵州","https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E8%B4%B5%E5%B7%9E&stage=publish&callback=jsonp_1613639151816_46268"),
    yunnan("云南","https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E4%BA%91%E5%8D%97&stage=publish&callback=jsonp_1613639192392_8955"),
    fujian("福建","https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E7%A6%8F%E5%BB%BA&stage=publish&callback=jsonp_1613639224673_14823"),
    guangdong("广东","https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E5%B9%BF%E4%B8%9C&stage=publish&callback=jsonp_1613639262719_92425"),
    guangxi("广西","https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E5%B9%BF%E8%A5%BF&stage=publish&callback=jsonp_1613639276800_24319"),
    hainan("海南","https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E6%B5%B7%E5%8D%97&stage=publish&callback=jsonp_1613639440142_18146"),
    taiwan("台湾",""),
    aomen("澳门",""),
    xianggang("香港","")
    ;

    AreaUrlEnum(String area, String url) {
        this.area = area;
        this.url = url;
    }

    private String area;
    private String url;

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
