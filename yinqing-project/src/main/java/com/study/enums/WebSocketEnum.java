package com.study.enums;

/**
 * @Author: liyuankun
 * @Date: 2021/2/7 14:43
 * @Description:
 **/
public enum WebSocketEnum {

    GetBasicData("2", "获取基础数据"),
    GetBasicDataAdd("1", "获取新增基础数据"),
    AddNewTrend("3", "添加新增趋势")
    ;

    private String code;
    private String message;

    WebSocketEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
