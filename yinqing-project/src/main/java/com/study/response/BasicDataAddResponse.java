package com.study.response;


import com.study.bo.BasicDataAddBO;
import com.study.common.bo.BaseResponse;

import java.util.ArrayList;

public class BasicDataAddResponse extends BaseResponse {

    private static final long serialVersionUID = 1L;


    private ArrayList<BasicDataAddBO> list = new ArrayList<BasicDataAddBO>();

    public ArrayList<BasicDataAddBO> getList() {
        return list;
    }

    public void setList(ArrayList<BasicDataAddBO> list) {
        this.list = list;
    }

}
