package com.study.response;


import com.study.bo.DomesticInformationBO;
import com.study.bo.NewTrendBO;
import com.study.common.bo.BaseResponse;

import java.util.ArrayList;

public class DomesticInformationResponse extends BaseResponse {

    private static final long serialVersionUID = 1L;


    private ArrayList<DomesticInformationBO> list = new ArrayList<DomesticInformationBO>();

    public ArrayList<DomesticInformationBO> getList() {
        return list;
    }

    public void setList(ArrayList<DomesticInformationBO> list) {
        this.list = list;
    }

}
