package com.study.response;


import com.study.bo.AreaDataBO;
import com.study.common.bo.BaseResponse;

import java.util.ArrayList;

public class AreaDataResponse extends BaseResponse {

    private static final long serialVersionUID = 1L;


    private ArrayList<AreaDataBO> list = new ArrayList<AreaDataBO>();

    public ArrayList<AreaDataBO> getList() {
        return list;
    }

    public void setList(ArrayList<AreaDataBO> list) {
        this.list = list;
    }

}
