package com.study.utils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.*;
import com.study.bo.AreaDataBO;
import com.study.common.utils.DateTimeUtil;
import com.study.common.utils.sortUtil;
import com.study.enums.AreaUrlEnum;
import com.study.impl.DomesticInformationServiceImpl;
import com.study.pojo.AreaData;
import com.study.pojo.CumulativeTrend;
import com.study.pojo.DomesticInformation;
import lombok.SneakyThrows;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;

/**
 * @Author: liyuankun
 * @Date: 2021/1/18 16:43
 * @Description: 爬虫工具类
 **/
public class HtmlParseUtils {

    private static final Logger logger = LoggerFactory.getLogger(HtmlParseUtils.class);
    public static void main(String[] args) throws Exception {

//        String url = "https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E9%BB%91%E9%BE%99%E6%B1%9F&stage=publish&callback=jsonp_1613633348680_3577";
//        String url = "https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E5%90%89%E6%9E%97&stage=publish&callback=jsonp_1613637249909_26237";
        String url = "https://voice.baidu.com/newpneumonia/getv2?target=trend&isCaseIn=1&from=mola-virus&area=%E5%90%89%E6%9E%97&stage=publish&callback=jsonp_1613640462140_23156";
//        parse(url);
//        System.out.println(JSONObject.toJSONString(jsonToMapAreaData(url)));
        jsonToMapBasicData();
    }


    // 解析 https://voice.baidu.com/act/newpneumonia/newpneumonia/#tab1
    @SneakyThrows
    public static Document parse(String url) {
        LogFactory.getFactory().setAttribute("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");
        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(Level.OFF);
        java.util.logging.Logger.getLogger("org.apache.commons.httpclient").setLevel(Level.OFF);
        final WebClient webClient = new WebClient(BrowserVersion.CHROME);//新建一个模拟谷歌Chrome浏览器的浏览器客户端对象
        webClient.getOptions().setThrowExceptionOnScriptError(false);//当JS执行出错的时候是否抛出异常, 这里选择不需要
        webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);//当HTTP的状态非200时是否抛出异常, 这里选择不需要
        webClient.getOptions().setActiveXNative(false);
        webClient.getOptions().setCssEnabled(false);//是否启用CSS, 因为不需要展现页面, 所以不需要启用
        webClient.getOptions().setJavaScriptEnabled(true); //很重要，启用JS
        webClient.setAjaxController(new NicelyResynchronizingAjaxController());//很重要，设置支持AJAX

        HtmlPage page = null;
        try {
            page = webClient.getPage(new URL(url));//尝试加载上面图片例子给出的网页
            webClient.waitForBackgroundJavaScript(5000);//异步JS执行需要耗时,所以这里线程要阻塞30秒,等待异步JS执行结束
        } catch (Exception e) {
            logger.error("<<<<<< parse error cause", e);
        }finally {
            webClient.close();
        }

        String pageXml = page.asXml();//直接将加载完成的页面转换成xml格式的字符串
        System.out.println(pageXml);
        Document document = Jsoup.parse(pageXml);//获取html文档

        return document;
    }

    //各地区数据
    public static AreaData jsonToMapAreaData(String url) throws IOException {
        AreaData areaData = new AreaData();
        InputStream is = new URL(url).openStream();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
        String jsonText = readAll(rd);
        int start = jsonText.indexOf("success") + 17;
        int last = jsonText.length() - 4;
        String substring = jsonText.substring(start, last);
        Map map = JSON.parseObject(substring,Map.class);
        //区域名字
        areaData.setArea((String) map.get("name"));
        Map trendMap = JSON.parseObject(map.get("trend").toString() ,Map.class);
        List<AreaDataBO> list = JSONArray.parseArray(trendMap.get("list").toString()).toJavaList(AreaDataBO.class);
        //取最后一个数据
        int size = list.get(0).getData().size();
        //累计确诊
        String cumulativeNumber = String.valueOf(list.get(0).getData().get(size - 1));
        areaData.setCumulativeNumber(cumulativeNumber);
        //累计治愈
        String cureNumber = String.valueOf(list.get(1).getData().get(size - 1));
        areaData.setCureNumber(cureNumber);
        //累计死亡
        String deathNumber = String.valueOf(list.get(2).getData().get(size - 1));
        areaData.setDeathNumber(deathNumber);
        //新增确诊
        String newNumber = String.valueOf(list.get(3).getData().get(size - 1));
        areaData.setNewNumber(newNumber);
        //现有确诊
        int existingNumber = Integer.parseInt(cumulativeNumber) - (Integer.parseInt(cureNumber) + Integer.parseInt(deathNumber));
        areaData.setExistingNumber(String.valueOf(existingNumber));
        //时间
        areaData.setDate(DateTimeUtil.getNow("yyyy-MM-dd"));
        return areaData;
    }

    //基础数据
    public static List<String> jsonToMapBasicData(){

        String url = "https://voice.baidu.com/act/newpneumonia/newpneumonia/#tab1";

        Document document = parse(url);

        String text1 = document.getElementsByClass("mola-window").get(0).child(0).tagName("div").className();

        String substring1 = text1.substring(6, 13);

        ArrayList<String> list = new ArrayList<>();
        //时间

        String timeString = "Virus_1-1-295_32Y_aO";
        String timeStringReplace = timeString.replace(timeString.substring(6, 13), substring1);
        Elements elementsDate = document.getElementsByClass(timeStringReplace);
        for (Element date : elementsDate) {
            String span = date.getElementsByTag("span").eq(0).text();
            String substring = span.substring(6);
            list.add(substring);
        }

        //基础数据
        String dataString = "VirusSummarySix_1-1-295_2ZJJBJ";
        String dataStringReplace = dataString.replace(dataString.substring(16, 23), substring1);
        Elements elementsData = document.getElementsByClass(dataStringReplace);
        for (Element elementsDatum : elementsData) {
            String text = elementsDatum.text();
            list.add(text);
        }
        //新增数据
        String addDataString = "VirusSummarySix_1-1-300_nfO6Mw";
        String addDataStringReplace = addDataString.replace(addDataString.substring(16, 23), substring1);
        Elements elementsAdd = document.getElementsByClass(addDataStringReplace);
        for (Element element : elementsAdd) {
            String span = element.getElementsByTag("span").eq(0).text();
            list.add(span);
        }

        return list;
    }

    //区域数据
    public static List<AreaData> jsonToMapAreaData(){

        String url = "https://voice.baidu.com/act/newpneumonia/newpneumonia/#tab1";
        Document document = parse(url);

        //取到更新的class
        String text1 = document.getElementsByClass("mola-window").get(0).child(0).tagName("div").className();
        String substring1 = text1.substring(6, 13);

        List<AreaData> areaList = new ArrayList<>();

        //基础数据
        String dataString = "VirusTable_1-1-296_3m6Ybq";
        String dataStringReplace = dataString.replace(dataString.substring(11, 18), substring1);
        Elements elementsData = document.getElementsByClass(dataStringReplace);
        for (Element elementsDatum : elementsData) {
            AreaData areaData = new AreaData();
            List<String> list = new ArrayList<>();
            String text = elementsDatum.text();
            String [] arr = text.split("\\s+");
            for (int i = 0; i < arr.length; i++) {
                list.add(arr[i]);
            }
            areaData.setArea(list.get(0));
            areaData.setNewNumber(list.get(1));
            areaData.setExistingNumber(list.get(2));
            areaData.setCumulativeNumber(list.get(3));
            areaData.setCureNumber(list.get(4));
            areaData.setDeathNumber(list.get(5));
            areaData.setDate(DateTimeUtil.getNow("yyyy-MM-dd"));
            areaList.add(areaData);
        }
        List<AreaData> list = new ArrayList<>();
        for (int i = 0; i < areaList.size(); i++) {
            if (StringUtils.equals(areaList.get(i).getArea(), AreaUrlEnum.taiwan.getArea())) list.add(areaList.get(i));
            if (StringUtils.equals(areaList.get(i).getArea(), AreaUrlEnum.aomen.getArea())) list.add(areaList.get(i));
            if (StringUtils.equals(areaList.get(i).getArea(), AreaUrlEnum.xianggang.getArea())) list.add(areaList.get(i));
        }
        return list;
    }

    // 新增趋势
    public static Map<String,List<String>> jsonToMapNewTrend(String url) throws IOException {
        InputStream is = new URL(url).openStream();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
        String jsonText = readAll(rd);
        JSONObject jsonObject = JSONObject.parseObject(jsonText);
        List<String> list = jsonObject.getJSONArray("confirm_add").toJavaList(String.class);
        List<String> list1 = jsonObject.getJSONArray("day").toJavaList(String.class);
        List<String> list2 = jsonObject.getJSONArray("heal_add").toJavaList(String.class);
        List<String> list3 = jsonObject.getJSONArray("dead_add").toJavaList(String.class);
        List<String> list4 = jsonObject.getJSONArray("suspect_add").toJavaList(String.class);
        Map<String, List<String>> map = new HashMap<>();
        map.put("confirm_add",list);
        map.put("day",list1);
        map.put("heal_add",list2);
        map.put("dead_add",list3);
        map.put("suspect_add",list3);
        return map;
    }

    // 累计趋势
    public static Map<String,List<String>> jsonToMapCumulativeTrend(String url) throws IOException {
        InputStream is = new URL(url).openStream();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
        String jsonText = readAll(rd);
        JSONObject jsonObject = JSONObject.parseObject(jsonText);
        List<String> list = jsonObject.getJSONArray("confirm").toJavaList(String.class);
        List<String> list1 = jsonObject.getJSONArray("day").toJavaList(String.class);
        List<String> list2 = jsonObject.getJSONArray("heal").toJavaList(String.class);
        List<String> list3 = jsonObject.getJSONArray("dead").toJavaList(String.class);
        Map<String, List<String>> map = new HashMap<>();
        map.put("confirm",list);
        map.put("day",list1);
        map.put("heal",list2);
        map.put("dead",list3);
        return map;
    }

    // 国内资讯
    public static List<DomesticInformation> jsonToMapDomesticInformation(String url) throws IOException {
        InputStream is = new URL(url).openStream();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
        String jsonText = readAll(rd);
        int start = jsonText.indexOf("items") + 7;
        int last = jsonText.indexOf("title") - 2;
        String substring = jsonText.substring(start, last);
        List<DomesticInformation> list = JSON.parseArray(substring).toJavaList(DomesticInformation.class);
        sortUtil sortUtil = new sortUtil();
        Collections.sort(list,sortUtil);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (DomesticInformation domesticInformation : list) {
            Long time = Long.parseLong(String.valueOf(Integer.parseInt(domesticInformation.getEventTime()))) * 1000L;
            String date = simpleDateFormat.format(new Date(time));
            domesticInformation.setEventTime(date);
        }
        return list;
    }


    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

}
