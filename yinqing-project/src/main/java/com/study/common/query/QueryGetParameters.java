package com.study.common.query;

import java.util.Map;

public interface QueryGetParameters {
	/**
	 * 获取查询参数
	 * @return
	 */
	  public abstract Map<String, String> getParameters();
}
