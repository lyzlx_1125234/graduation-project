package com.study.common.query;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;


public class QueryPage extends QueryBase implements QueryGetParameters
{
  //private static final Logger logger = LoggerFactory.getLogger(QueryPage.class);
	
  private static final long serialVersionUID = -4734192696485746607L;
  Object obj;
  String queryPageClose;
  List<?> items;

  public QueryPage()
  {
  }

  public QueryPage(Object obj)
  {
    this.obj = obj;
  }

  @Override
  public Map<String, String> getParameters()
  {
//    Assert.notNull(this.obj);
//    Class clazz = this.obj.getClass();
//    HashMap resMap = new HashMap();
//    try {
//      HashMap map = new HashMap();
//      getClass(clazz, map, this.obj);
//      resMap = convertHashMap(map);
//    } catch (Exception e) {
//      if (logger.isErrorEnabled()) {
//        logger.error("{}", e);
//      }
//    }
    return new HashMap();
  }

  private static void getClass(Class clazz, HashMap map, Object obj)
    throws Exception
  {
	String classFlag = "Object";
    if (StringUtils.equals(clazz.getSimpleName(), classFlag)) {
      return;
    }
    Field[] fields = clazz.getDeclaredFields();
    if ((fields != null) && (fields.length > 0)) {
      for (int i = 0; i < fields.length; i++) {
        fields[i].setAccessible(true);
        String name = fields[i].getName();
        if (!"serialVersionUID".equals(name))
        {
          Object value = fields[i].get(obj);
          map.put(name, value);
        }
      }
    }
    Class superClzz = clazz.getSuperclass();
    getClass(superClzz, map, obj);
  }

  private static HashMap<String, String> convertHashMap(HashMap map)
    throws Exception
  {
    HashMap newMap = new HashMap();
    Set keys = map.keySet();
    Iterator it = keys.iterator();
    while (it.hasNext()) {
      Object key = it.next();
      convertToString(map.get(key), newMap, key);
    }

    return newMap;
  }

  private static void convertToString(Object value, HashMap newMap, Object key)
  {
    if (value != null) {
      Class clazz = value.getClass();
      if (isBaseType(clazz)) {
        newMap.put(key, value.toString());
      } else if (clazz == String.class) {
        newMap.put(key, value.toString());
      } else if (clazz == java.util.Date.class) {
        java.util.Date date = (java.util.Date)value;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        newMap.put(key, sdf.format(date));
      } else if (clazz == Timestamp.class) {
        Timestamp timestamp = (Timestamp)value;
        long times = timestamp.getTime();
        java.util.Date date = new java.util.Date(times);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        newMap.put(key, sdf.format(date));
      } else if (clazz == java.sql.Date.class) {
        java.sql.Date sqlDate = (java.sql.Date)value;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        newMap.put(key, sdf.format(sqlDate));
      } else {
        newMap.put(key, value);
      }
    }
  }

  private static boolean isBaseType(Class clazz)
  {
    boolean isBaseType = false;

    if (clazz == Integer.class) {
      isBaseType = true;
    }
    if (clazz == Long.class) {
      isBaseType = true;
    }
    if (clazz == Double.class) {
      isBaseType = true;
    }
    if (clazz == Byte.class) {
      isBaseType = true;
    }
    if (clazz == Float.class) {
      isBaseType = true;
    }
    if (clazz == Short.class) {
      isBaseType = true;
    }
    if (clazz == Boolean.class) {
      isBaseType = true;
    }
    return isBaseType;
  }

  public List<?> getItems()
  {
    return this.items;
  }

  public void setItems(List<?> items)
  {
    this.items = items;
  }

  public Object getObj()
  {
    return this.obj;
  }

  public void setObj(Object obj)
  {
    this.obj = obj;
  }

  public String getQueryPageClose()
  {
    return this.queryPageClose;
  }

  public void setQueryPageClose()
  {
    this.queryPageClose = "TRUE";
  }
}