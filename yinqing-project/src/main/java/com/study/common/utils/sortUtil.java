package com.study.common.utils;

import com.study.pojo.DomesticInformation;

import java.util.Comparator;

/**
 * @Author: liyuankun
 * @Date: 2021/1/26 11:25
 * @Description:
 **/
public class sortUtil implements Comparator {
    @Override
    public int compare(Object o1, Object o2) {
        DomesticInformation domesticInformation1 = (DomesticInformation)o1;
        DomesticInformation domesticInformation2 = (DomesticInformation)o2;
        int flag = domesticInformation1.getEventTime().compareTo(domesticInformation2.getEventTime());
        return flag;
    }
}
