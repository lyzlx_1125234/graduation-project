/*
 * Hundsun Inc.
 * Copyright (c) 2006-2017 All Rights Reserved.
 *
 * Author     :zhangx
 * Version    :1.0
 * Create Date:2017-11-6
 *
 */
package com.study.common.utils;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cglib.beans.BeanMap;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author zhangx
 * @version $Id: NsfBaseUtil.java 2017-11-6 下午3:00:58 zhangx $
 */
public class BeanConvertUtil {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(BeanConvertUtil.class);

	/*** 日期区 begin ***/

	public static Long getLongDate(String date) {
		return Long.parseLong(getStrDate(date));
	}

	public static String getStrDate(String date) {
		date = date.replaceAll("\\W", "");
		int dateLength = 8;
		if (StringUtils.length(date) == dateLength) {
			date = date + "000000";
		}
		int dateTimeLength = 14;
		if (StringUtils.length(date) != dateTimeLength) {
			logger.error("<<<<<< data format only support yyyy-MM-dd or yyyy-MM-dd hh:mm:ss");
			date = "0";
		}
		return date;
	}

	private static final String DEFAULT_DT_PATTERN = "yyyyMMddHHmmss";

	private static final Map<String, SimpleDateFormat> SDF_MAP = new HashMap<String, SimpleDateFormat>();

	public static Date convertLong2Date(Long time) throws ParseException {
		String formatStr = time.toString();
		return getDateFormat(DEFAULT_DT_PATTERN).parse(formatStr);
	}

	private static SimpleDateFormat getDateFormat(String pattern) {
		if (!SDF_MAP.containsKey(pattern)) {
			SimpleDateFormat format = new SimpleDateFormat(pattern);
			synchronized (SDF_MAP) {
				SDF_MAP.put(pattern, format);
			}
		}
		return SDF_MAP.get(pattern);
	}

	/*** 日期区 begin ***/

	/*** bean转换 begin ***/

	/**
	 * bean转map
	 * 
	 * @param bean
	 * @return
	 */
	public static <T> Map<String, Object> bean2Map(T bean) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (bean != null) {
			BeanMap beanMap = BeanMap.create(bean);
			for (Object key : beanMap.keySet()) {
				//System.out.println("key===="+key);
				if (beanMap.get(key) instanceof String) {
					map.put(key.toString(), StringUtils.trim(String.valueOf(beanMap.get(key))));
				} else {
					map.put(key.toString(), beanMap.get(key));
				}

			}
		}
		return map;
	}

	/**
	 * javabean转成map
	 * 
	 * @param bean
	 * @param isHump
	 *            ture=下划线转驼峰 false=驼峰转下划线
	 * @return
	 */
	public static <T> Map<String, Object> bean2Map(T bean, boolean isHump) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (bean != null) {
			BeanMap beanMap = BeanMap.create(bean);
			for (Object key : beanMap.keySet()) {
				if (isHump) {
					if (beanMap.get(key) instanceof String) {
						map.put(underlineToHump((String) key), StringUtils.trim(String.valueOf(beanMap.get(key))));
					} else {
						map.put(underlineToHump((String) key), beanMap.get(key));
					}

				} else {
					if (beanMap.get(key) instanceof String) {
						map.put(humpToUnderLine((String) key), StringUtils.trim(String.valueOf(beanMap.get(key))));
					} else {
						map.put(humpToUnderLine((String) key), beanMap.get(key));
					}
				}
			}
		}
		return map;
	}

	public static <T> Map<String, Object> bean2MapUnderline(T bean) {
		return bean2Map(bean, false);
	}

	public static <T> Map<String, Object> bean2MapHump(T bean) {
		return bean2Map(bean, true);
	}

	/**
	 * 将map装换为javabean对象
	 * 
	 * @param map
	 * @param bean
	 * @return
	 */
	public static <T> T mapToBean(Map<String, Object> map, T bean) {
		BeanMap beanMap = BeanMap.create(bean);

		// beanMap.putAll(map);
		for (Object key : beanMap.keySet()) {
			try {
				beanMap.put(key, map.get(key));
			} catch (ClassCastException e) {
				Object[] ary = { bean.getClass().toString(), key, map.get(key) };
				logger.debug("<<<<<< class cast error class={}, key={}, value={}", ary);
			} catch (Exception e) {
				if (StringUtils.equals("gmt_modify", String.valueOf(key)) || StringUtils.equals("gmt_create", String.valueOf(key))) {
					logger.info("无需处理");
				} else {
					Object[] ary = { bean.getClass().toString(), key, map.get(key) };
					logger.error("<<<<<< class cast error class={}, key={}, value={}", ary);
				}
			}
		}

		return bean;
	}

	private static Pattern linePattern = Pattern.compile("_(\\w)");

	/** 下划线转驼峰 */
	public static String underlineToHump(String str) {
		str = str.toLowerCase();
		Matcher matcher = linePattern.matcher(str);
		StringBuffer sb = new StringBuffer();
		while (matcher.find()) {
			matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
		}
		matcher.appendTail(sb);
		return sb.toString();
	}

	private static Pattern humpPattern = Pattern.compile("[A-Z]");

	/** 驼峰转下划线 */
	public static String humpToUnderLine(String str) {
		Matcher matcher = humpPattern.matcher(str);
		StringBuffer sb = new StringBuffer();
		while (matcher.find()) {
			matcher.appendReplacement(sb, "_" + matcher.group(0).toLowerCase());
		}
		matcher.appendTail(sb);
		return sb.toString();
	}

	/*** bean转换 end ***/

	public static void main(String[] args) {
//		String date = "201711060 ";
	}

}
