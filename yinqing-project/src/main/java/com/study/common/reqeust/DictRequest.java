package com.study.common.reqeust;

import com.study.common.query.QueryPage;

import java.util.List;


/**
 * <p>
 * 字典查询对象
 * </p>
 * 
 * @author zhangx
 * @version $Id: DictRequest.java 2020年3月16日 上午10:17:54 zhangx $
 */
public class DictRequest extends QueryPage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2482371975199173226L;

	private String dicttype;
	private String serino;
	private String value;
	private String reserved1;
	private String stat;
	private List<String> serinoAry;

	

	public List<String> getSerinoAry() {
		return serinoAry;
	}

	public void setSerinoAry(List<String> serinoAry) {
		this.serinoAry = serinoAry;
	}

	public String getStat() {
		return stat;
	}

	public void setStat(String stat) {
		this.stat = stat;
	}

	public String getDicttype() {
		return dicttype;
	}

	public void setDicttype(String dicttype) {
		this.dicttype = dicttype;
	}

	public String getSerino() {
		return serino;
	}

	public void setSerino(String serino) {
		this.serino = serino;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getReserved1() {
		return reserved1;
	}

	public void setReserved1(String reserved1) {
		this.reserved1 = reserved1;
	}

}