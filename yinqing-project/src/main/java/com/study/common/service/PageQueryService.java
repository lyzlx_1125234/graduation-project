/*
 * Hundsun Inc.
 * Copyright (c) 2006-2016 All Rights Reserved.
 *
 * Author     :zhangx
 * Version    :1.0
 * Create Date:2016-11-10
 *
 */
package com.study.common.service;

import java.util.Map;

import com.study.common.query.QueryPage;
import com.study.common.utils.InitValueConstant;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;


/**
 * 分页服务基类
 * 
 * @author zhangx
 * @version $Id: PageQueryService.java 2016-11-10 上午9:27:35 zhangx $
 */
public class PageQueryService {

	@Value("hs.datasource.default.driver-class-name")
	private String dataBaseType;

	public void setPageParams(Map<String, Object> map, QueryPage page) {
		
		page.setCurrentPage(page.getPageNo());
		
		// 设置分页参数 查询分页记录
		if (StringUtils.contains(dataBaseType, InitValueConstant.MYSQL)) {
			map.put(InitValueConstant.STARTINDEX, page.getMysqlPageFristItem());
			map.put(InitValueConstant.PAGENO, page.getPageSize());
		} else {
			map.put(InitValueConstant.PAGEFRISTITEM, page.getPageFristItem());
			map.put(InitValueConstant.PAGELASTITEM, page.getPageLastItem());
		}

	}

}
