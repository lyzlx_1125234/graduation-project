package com.study.service;

import com.study.request.BasicDataRequest;
import com.study.request.DomesticInformationRequest;
import com.study.response.BasicDataResponse;
import com.study.response.DomesticInformationResponse;
import org.springframework.stereotype.Service;

/**
 * @Author: liyuankun
 * @Date: 2021/1/28 15:18
 * @Description:  基础数据服务
 **/

@Service
public interface BasicDataService {

    public BasicDataResponse queryBasicData(BasicDataRequest request);

    public BasicDataResponse getBasicData();

    public BasicDataResponse queryBasicDataLast();

}
