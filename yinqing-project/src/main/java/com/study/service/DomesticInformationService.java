package com.study.service;

import com.study.request.CumulativeTrendRequest;
import com.study.request.DomesticInformationRequest;
import com.study.response.DomesticInformationResponse;
import com.study.response.NewTrendResponse;
import org.springframework.stereotype.Service;

/**
 * @Author: liyuankun
 * @Date: 2021/1/25 21:20
 * @Description: 国内资讯
 **/
@Service
public interface DomesticInformationService {

    public DomesticInformationResponse queryDomesticInformation(DomesticInformationRequest request);

    // 定时任务，获取国内资讯
    public DomesticInformationResponse getDomesticInformation();

}
