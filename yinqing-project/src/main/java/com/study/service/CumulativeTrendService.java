package com.study.service;

import com.study.request.CumulativeTrendRequest;
import com.study.response.CumulativeTrendResponse;
import org.springframework.stereotype.Service;

/**
 * @Author: liyuankun
 * @Date: 2021/1/25 15:33
 * @Description: 全国疫情累计趋势
 **/
@Service
public interface CumulativeTrendService {


    public CumulativeTrendResponse queryCumulativeTrend(CumulativeTrendRequest request);

    public void getCumulativeTrend();

    public CumulativeTrendResponse addCumulativeTrend();

}
