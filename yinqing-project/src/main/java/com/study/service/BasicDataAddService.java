package com.study.service;

import com.study.request.BasicDataAddRequest;
import com.study.response.BasicDataAddResponse;
import org.springframework.stereotype.Service;

/**
 * @Author: liyuankun
 * @Date: 2021/1/28 15:18
 * @Description:  基础数据新增服务
 **/

@Service
public interface BasicDataAddService {

    public BasicDataAddResponse queryBasicAddData(BasicDataAddRequest request);

    public BasicDataAddResponse getBasicDataAdd();

    public BasicDataAddResponse queryBasicDataAddLast();

}
