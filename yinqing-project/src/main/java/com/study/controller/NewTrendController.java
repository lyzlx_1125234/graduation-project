package com.study.controller;

import com.alibaba.fastjson.JSONObject;
import com.study.common.emus.CommonErrorCodeEnum;
import com.study.request.BasicDataRequest;
import com.study.request.NewTrendRequest;
import com.study.response.BasicDataResponse;
import com.study.response.NewTrendResponse;
import com.study.service.NewTrendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: liyuankun
 * @Date: 2021/2/6 16:58
 * @Description:  新增趋势
 **/
@RestController
@RequestMapping("/api/newTrend")
public class NewTrendController {

    @Autowired
    private NewTrendService newTrendService;

    /**
     * 新增趋势查询
     * @param request
     * @return
     */
    @RequestMapping(value = "/select",produces="application/json;charset=utf-8")
    public String queryNewTrend(NewTrendRequest request){
        NewTrendResponse response = null;
        try {
            response = newTrendService.queryNewTrend(request);
        }catch(Exception e) {
            response = new NewTrendResponse();
            response.setCode(CommonErrorCodeEnum.ERROR.getCode());
            response.setMessage(CommonErrorCodeEnum.ERROR.getMessage());
        }
        return JSONObject.toJSONString(response);
    }

}
