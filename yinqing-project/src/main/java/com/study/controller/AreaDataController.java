package com.study.controller;

import com.alibaba.fastjson.JSONObject;
import com.study.common.emus.CommonErrorCodeEnum;
import com.study.common.utils.DateTimeUtil;
import com.study.request.AreaDataRequest;
import com.study.request.BasicDataAddRequest;
import com.study.response.AreaDataResponse;
import com.study.response.BasicDataAddResponse;
import com.study.service.AreaDataService;
import com.study.service.BasicDataAddService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: liyuankun
 * @Date: 2021/2/15 15:20
 * @Description: 区域数据
 **/
@RestController
@RequestMapping("/api/areaData")
public class AreaDataController {

    @Autowired
    private AreaDataService areaDataService;

    /**
     * 区域数据最新查询
     * @param request
     * @return
     */
    @RequestMapping(value = "/select",produces="application/json;charset=utf-8")
    public String queryAreaData(AreaDataRequest request){
        AreaDataResponse response = null;
        try {
            request.setDate(DateTimeUtil.getNow("yyyy-MM-dd"));
            response = areaDataService.queryAreaData(request);
            if (response.getList().size() == 0) {
                request.setDate(DateTimeUtil.getYesterday());
                response = areaDataService.queryAreaData(request);
            }
        }catch(Exception e) {
            response = new AreaDataResponse();
            response.setCode(CommonErrorCodeEnum.ERROR.getCode());
            response.setMessage(CommonErrorCodeEnum.ERROR.getMessage());
        }
        return JSONObject.toJSONString(response);
    }

}
