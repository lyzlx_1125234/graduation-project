package com.study.controller;

import com.alibaba.fastjson.JSONObject;
import com.study.common.emus.CommonErrorCodeEnum;
import com.study.common.utils.DateTimeUtil;
import com.study.request.DomesticInformationRequest;
import com.study.response.DomesticInformationResponse;
import com.study.service.DomesticInformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: liyuankun
 * @Date: 2021/2/21 17:41
 * @Description:   国内资讯
 **/
@RestController
@RequestMapping("/api/domesticInformation")
public class DomesticInformationController {

    @Autowired
    private DomesticInformationService domesticInformationService;

    /**
     * 国内资讯查询
     * @param request
     * @return
     */
    @RequestMapping(value = "/select",produces="application/json;charset=utf-8")
    public String queryDomesticInformation(DomesticInformationRequest request){
        DomesticInformationResponse response = null;
        try {
            request.setEventTime(DateTimeUtil.getNow("yyyy-MM-dd"));
            response = domesticInformationService.queryDomesticInformation(request);
        }catch(Exception e) {
            response = new DomesticInformationResponse();
            response.setCode(CommonErrorCodeEnum.ERROR.getCode());
            response.setMessage(CommonErrorCodeEnum.ERROR.getMessage());
        }
        return JSONObject.toJSONString(response);
    }

}
