package com.study.controller;
import com.alibaba.fastjson.JSONObject;
import com.study.common.emus.CommonErrorCodeEnum;
import com.study.request.BasicDataRequest;
import com.study.response.BasicDataResponse;
import com.study.service.BasicDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: liyuankun
 * @Date: 2021/1/31 16:12
 * @Description: 基础数据
 **/

@RestController
@RequestMapping("/api/basicData")
public class BasicDataController {

    @Autowired
    private BasicDataService basicDataService;

    /**
     * 基础数据最新查询
     * @param request
     * @return
     */
    @RequestMapping(value = "/select",produces="application/json;charset=utf-8")
    public String queryBasicData(BasicDataRequest request){
        BasicDataResponse response = null;
        try {
            response = basicDataService.queryBasicDataLast();
        }catch(Exception e) {
            response = new BasicDataResponse();
            response.setCode(CommonErrorCodeEnum.ERROR.getCode());
            response.setMessage(CommonErrorCodeEnum.ERROR.getMessage());
        }
        return JSONObject.toJSONString(response);
    }

}
