package com.study.impl;

import com.alibaba.fastjson.JSON;
import com.study.bo.NewTrendBO;
import com.study.common.emus.CommonErrorCodeEnum;
import com.study.common.utils.BeanConvertUtil;
import com.study.common.utils.DateTimeUtil;
import com.study.dao.NewTrendMapper;
import com.study.enums.WebSocketEnum;
import com.study.pojo.NewTrend;
import com.study.request.NewTrendRequest;
import com.study.response.NewTrendResponse;
import com.study.service.NewTrendService;
import com.study.service.WebSocket;
import com.study.utils.HtmlParseUtils;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: liyuankun
 * @Date: 2021/1/25 19:50
 * @Description: 全国疫情新增趋势
 **/
@Component
public class NewTrendServiceImpl implements NewTrendService {

    private static final Logger logger = LoggerFactory.getLogger(NewTrendServiceImpl.class);


    @Autowired
    private NewTrendMapper newTrendMapper;

    @Autowired
    private WebSocket webSocket;


    @Override
    public NewTrendResponse queryNewTrend(NewTrendRequest request) {
        NewTrendResponse response = new NewTrendResponse();
        response.setCode(CommonErrorCodeEnum.ERROR.getCode());
        response.setMessage(CommonErrorCodeEnum.ERROR.getMessage());
        try {
            // 数据信息
            List<NewTrend> list = newTrendMapper.getList(BeanConvertUtil.bean2Map(request));
            if (CollectionUtils.isNotEmpty(list)) {
                ArrayList<NewTrendBO> boList = new ArrayList<NewTrendBO>();
                for (NewTrend newTrend : list) {
                    NewTrendBO bo = BeanConvertUtil.mapToBean(BeanConvertUtil.bean2Map(newTrend), new NewTrendBO());
                    boList.add(bo);
                }
                response.setList(boList);
            }
            response.setCode(CommonErrorCodeEnum.SUCCESS.getCode());
            response.setMessage(CommonErrorCodeEnum.SUCCESS.getCode());
        } catch (IllegalArgumentException e) {
            response.setCode(CommonErrorCodeEnum.PARAMS_NULL.getCode());
            response.setMessage(e.getMessage());
        } catch (Exception e) {
            // slf4j 打印异常
            logger.error("<<<<<< queryNewTrend error cause", e);
        }
        return response;
    }


    @Override
    public void getNewTrend() {

        try {
            Map<String, List<String>> map = HtmlParseUtils.jsonToMapNewTrend("http://aiiyx.cn:81/l2");

            for (int i = 0; i < map.get("confirm_add").size(); i++) {
                NewTrend newTrend = new NewTrend();
                newTrend.setNewlyDiagnosed(map.get("confirm_add").get(i));;
                newTrend.setDate(map.get("day").get(i));
                newTrend.setAddCure(map.get("heal_add").get(i));
                newTrend.setNewDeath(map.get("dead_add").get(i));
                newTrend.setAddSuspect(map.get("suspect_add").get(i));
                newTrendMapper.insert(newTrend);
            }

        } catch (IOException e) {
            logger.error("<<<<<< getCumulativeTrend error cause", e);
        }

    }

    @Override
    public NewTrendResponse addNewTrend() {
        NewTrendResponse response = new NewTrendResponse();
        response.setCode(CommonErrorCodeEnum.ERROR.getCode());
        response.setMessage(CommonErrorCodeEnum.ERROR.getMessage());
        try {
            NewTrendResponse newTrendResponse = queryNewTrend(new NewTrendRequest());
            List<String> descList = new ArrayList<>();
            for (NewTrendBO newTrendBO : newTrendResponse.getList()) {
                descList.add(newTrendBO.getDate());
            }
            List<String> list = HtmlParseUtils.jsonToMapBasicData();
            List<NewTrend> newTrendList = new ArrayList<>();
            Map<String, Object> map = new HashMap<>();
            if (!descList.contains(DateTimeUtil.getYesterday())) {
                NewTrend newTrend = new NewTrend();
                newTrend.setDate(DateTimeUtil.getYesterday());
                newTrend.setAddSuspect(list.get(11).substring(1));
                newTrend.setNewlyDiagnosed(list.get(13).substring(1));
                newTrend.setAddCure(list.get(15).substring(1));
                newTrend.setNewDeath(list.get(16).substring(1));
                newTrendList.add(newTrend);
            }else {
                response.setCode(CommonErrorCodeEnum.SUCCESS.getCode());
                response.setMessage(CommonErrorCodeEnum.SUCCESS.getMessage());
                return response;
            }
            map.put("newTrendList", newTrendList);
            int i = newTrendMapper.insertByForeach(map);
            if (i > 0) {
                //发送websocket消息
                NewTrendResponse lastResponse = queryNewTrend(new NewTrendRequest());
                lastResponse.setCode(WebSocketEnum.AddNewTrend.getCode());
                webSocket.sendMessage(JSON.toJSONString(lastResponse));
                response.setCode(CommonErrorCodeEnum.SUCCESS.getCode());
                response.setMessage(CommonErrorCodeEnum.SUCCESS.getMessage());
            }
        } catch (Exception e) {
            logger.error("addNewTrend  error cause", e);
        }
        return response;
    }
}
