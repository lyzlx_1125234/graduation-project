package com.study.impl;

import com.study.bo.DomesticInformationBO;
import com.study.common.emus.CommonErrorCodeEnum;
import com.study.common.utils.BeanConvertUtil;
import com.study.dao.DomesticInformationMapper;
import com.study.pojo.DomesticInformation;
import com.study.request.DomesticInformationRequest;
import com.study.response.DomesticInformationResponse;
import com.study.service.DomesticInformationService;
import com.study.utils.HtmlParseUtils;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: liyuankun
 * @Date: 2021/1/25 21:21
 * @Description: 国内资讯
 **/

@Component
public class DomesticInformationServiceImpl implements DomesticInformationService {

    private static final Logger logger = LoggerFactory.getLogger(DomesticInformationServiceImpl.class);

    @Autowired
    private DomesticInformationMapper domesticInformationMapper;
    
    @Autowired
    private DomesticInformationServiceImpl domesticInformationService;

    @Override
    public DomesticInformationResponse queryDomesticInformation(DomesticInformationRequest request) {
        DomesticInformationResponse response = new DomesticInformationResponse();
        response.setCode(CommonErrorCodeEnum.ERROR.getCode());
        response.setMessage(CommonErrorCodeEnum.ERROR.getMessage());
        try {
            // 数据信息
            List<DomesticInformation> list = domesticInformationMapper.getList(BeanConvertUtil.bean2Map(request));
            if (CollectionUtils.isNotEmpty(list)) {
                ArrayList<DomesticInformationBO> boList = new ArrayList<DomesticInformationBO>();
                for (DomesticInformation domesticInformation : list) {
                    DomesticInformationBO bo = BeanConvertUtil.mapToBean(BeanConvertUtil.bean2Map(domesticInformation), new DomesticInformationBO());
                    boList.add(bo);
                }
                response.setList(boList);
            }
            response.setCode(CommonErrorCodeEnum.SUCCESS.getCode());
            response.setMessage(CommonErrorCodeEnum.SUCCESS.getCode());
        } catch (IllegalArgumentException e) {
            response.setCode(CommonErrorCodeEnum.PARAMS_NULL.getCode());
            response.setMessage(e.getMessage());
        } catch (Exception e) {
            // slf4j 打印异常
            logger.error("<<<<<< queryDomesticInformation error cause", e);
        }
        return response;
    }

    @Override
    public DomesticInformationResponse getDomesticInformation() {
        DomesticInformationResponse response = new DomesticInformationResponse();
        response.setCode(CommonErrorCodeEnum.ERROR.getCode());
        response.setMessage(CommonErrorCodeEnum.ERROR.getMessage());
        String url = "https://opendata.baidu.com/data/inner?tn=reserved_all_res_tn&dspName=iphone&from_sf=1&dsp=iphone&resource_id=28565&alr=1&query=%E5%9B%BD%E5%86%85%E6%96%B0%E5%9E%8B%E8%82%BA%E7%82%8E%E6%9C%80%E6%96%B0%E5%8A%A8%E6%80%81&cb=jsonp_1611576450607_81346";
        try {
            DomesticInformationResponse domesticInformationResponse = domesticInformationService.queryDomesticInformation(new DomesticInformationRequest());
            List<String> descList = new ArrayList<>();
            for (DomesticInformationBO domesticInformationBO : domesticInformationResponse.getList()) {
                descList.add(domesticInformationBO.getEventDescription());
            }
            List<DomesticInformation> list = HtmlParseUtils.jsonToMapDomesticInformation(url);
            List<DomesticInformation> domesticInformationList = new ArrayList<>();
            Map<String, Object> map = new HashMap<>();
            for (DomesticInformation domesticInformation : list) {
                if (!descList.contains(domesticInformation.getEventDescription())) {
                    domesticInformationList.add(domesticInformation);
                }
            }
            if (domesticInformationList.size() == 0) {
                response.setCode(CommonErrorCodeEnum.SUCCESS.getCode());
                response.setMessage(CommonErrorCodeEnum.SUCCESS.getMessage());
                return response;
            }
            map.put("domesticInformationList",domesticInformationList);
            int i = domesticInformationMapper.insertByForeach(map);
            if (i > 0) {
                response.setCode(CommonErrorCodeEnum.SUCCESS.getCode());
                response.setMessage(CommonErrorCodeEnum.SUCCESS.getMessage());
            }
        } catch (IOException e) {
            logger.error("getDomesticInformation  error cause", e);
        }
        return response;
    }

}
