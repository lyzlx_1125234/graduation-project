package com.study.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.study.bo.CumulativeTrendBO;
import com.study.bo.NewTrendBO;
import com.study.common.emus.CommonErrorCodeEnum;
import com.study.common.utils.BeanConvertUtil;
import com.study.common.utils.DateTimeUtil;
import com.study.dao.CumulativeTrendMapper;
import com.study.enums.WebSocketEnum;
import com.study.pojo.CumulativeTrend;
import com.study.pojo.NewTrend;
import com.study.request.CumulativeTrendRequest;
import com.study.request.NewTrendRequest;
import com.study.response.CumulativeTrendResponse;
import com.study.response.NewTrendResponse;
import com.study.service.CumulativeTrendService;
import com.study.service.WebSocket;
import com.study.utils.HtmlParseUtils;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.*;

/**
 * @Author: liyuankun
 * @Date: 2021/1/25 15:49
 * @Description:
 **/
@Component
public class CumulativeTrendServiceImpl implements CumulativeTrendService {

    private static final Logger logger = LoggerFactory.getLogger(CumulativeTrendServiceImpl.class);

    @Autowired
    private CumulativeTrendMapper cumulativeTrendMapper;

    @Autowired
    private WebSocket webSocket;

    @Override
    public CumulativeTrendResponse queryCumulativeTrend(CumulativeTrendRequest request) {
        CumulativeTrendResponse response = new CumulativeTrendResponse();
        response.setCode(CommonErrorCodeEnum.ERROR.getCode());
        response.setMessage(CommonErrorCodeEnum.ERROR.getMessage());
        try {
            // 数据信息
            List<CumulativeTrend> list = cumulativeTrendMapper.getList(BeanConvertUtil.bean2Map(request));
            if (CollectionUtils.isNotEmpty(list)) {
                ArrayList<CumulativeTrendBO> boList = new ArrayList<CumulativeTrendBO>();
                for (CumulativeTrend cumulativeTrend : list) {
                    CumulativeTrendBO bo = BeanConvertUtil.mapToBean(BeanConvertUtil.bean2Map(cumulativeTrend), new CumulativeTrendBO());
                    boList.add(bo);
                }
                response.setList(boList);
            }
            response.setCode(CommonErrorCodeEnum.SUCCESS.getCode());
            response.setMessage(CommonErrorCodeEnum.SUCCESS.getCode());
        } catch (IllegalArgumentException e) {
            response.setCode(CommonErrorCodeEnum.PARAMS_NULL.getCode());
            response.setMessage(e.getMessage());
        } catch (Exception e) {
            // slf4j 打印异常
            logger.error("<<<<<< queryKeyWordListList error cause", e);
        }
        return response;
    }

    @Override
    public void getCumulativeTrend() {

        try {
            Map<String, List<String>> map = HtmlParseUtils.jsonToMapCumulativeTrend("http://aiiyx.cn:81/l1");

            for (int i = 0; i < map.get("confirm").size(); i++) {
                CumulativeTrend cumulativeTrend = new CumulativeTrend();
                cumulativeTrend.setCumulativeDiagnosis(map.get("confirm").get(i));;
                cumulativeTrend.setDate(map.get("day").get(i));
                cumulativeTrend.setCumulativeCure(map.get("heal").get(i));
                cumulativeTrend.setCumulativeDeaths(map.get("dead").get(i));
                cumulativeTrendMapper.insert(cumulativeTrend);
            }

        } catch (IOException e) {
            logger.error("<<<<<< getCumulativeTrend error cause", e);
        }

    }

    @Override
    public CumulativeTrendResponse addCumulativeTrend() {
        CumulativeTrendResponse response = new CumulativeTrendResponse();
        response.setCode(CommonErrorCodeEnum.ERROR.getCode());
        response.setMessage(CommonErrorCodeEnum.ERROR.getMessage());
        try {
            CumulativeTrendResponse cumulativeTrendResponse = queryCumulativeTrend(new CumulativeTrendRequest());
            List<String> descList = new ArrayList<>();
            for (CumulativeTrendBO cumulativeTrendBO : cumulativeTrendResponse.getList()) {
                descList.add(cumulativeTrendBO.getDate());
            }
            List<String> list = HtmlParseUtils.jsonToMapBasicData();
            List<CumulativeTrend> cumulativeTrendList = new ArrayList<>();
            Map<String, Object> map = new HashMap<>();
            if (!descList.contains(DateTimeUtil.getYesterday())) {
                CumulativeTrend cumulativeTrend = new CumulativeTrend();
                cumulativeTrend.setDate(DateTimeUtil.getYesterday());
                cumulativeTrend.setCumulativeDiagnosis(list.get(5).replace(",",""));
                cumulativeTrend.setCumulativeCure(list.get(7).replace(",",""));
                cumulativeTrend.setCumulativeDeaths(list.get(8).replace(",",""));
                cumulativeTrendList.add(cumulativeTrend);
            }else {
                response.setCode(CommonErrorCodeEnum.SUCCESS.getCode());
                response.setMessage(CommonErrorCodeEnum.SUCCESS.getMessage());
                return response;
            }
            map.put("cumulativeTrendList", cumulativeTrendList);
            int i = cumulativeTrendMapper.insertByForeach(map);
            if (i > 0) {
                //发送websocket消息
                CumulativeTrendResponse lastResponse = queryCumulativeTrend(new CumulativeTrendRequest());
                lastResponse.setCode(WebSocketEnum.AddNewTrend.getCode());
                webSocket.sendMessage(JSON.toJSONString(lastResponse));
                response.setCode(CommonErrorCodeEnum.SUCCESS.getCode());
                response.setMessage(CommonErrorCodeEnum.SUCCESS.getMessage());
            }
        } catch (Exception e) {
            logger.error("addCumulativeTrend  error cause", e);
        }
        return response;
    }
}
