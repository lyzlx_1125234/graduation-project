package com.study.job;

import com.study.impl.DomesticInformationServiceImpl;
import com.study.impl.NewTrendServiceImpl;
import com.study.service.NewTrendService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @Author: liyuankun
 * @Date: 2021/2/7 14:34
 * @Description: 新增趋势定时任务
 **/
@Component
public class NewTrendJob {

    private static final Logger logger = LoggerFactory.getLogger(NewTrendJob.class);

    @Autowired
    private NewTrendService newTrendServiceImpl;

    @Scheduled(cron = "0 0 10 * * ?")
    private void process(){

        try {
            newTrendServiceImpl.addNewTrend();
        } catch (Exception e) {
            logger.error("<<<<<< NewTrendJob  process  error cause", e);
        }


    }


}
