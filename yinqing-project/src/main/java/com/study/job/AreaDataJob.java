package com.study.job;

import com.study.common.utils.DateTimeUtil;
import com.study.pojo.AreaData;
import com.study.request.AreaDataRequest;
import com.study.service.AreaDataService;
import com.study.service.BasicDataAddService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @Author: liyuankun
 * @Date: 2021/2/15 15:15
 * @Description: 区域数据
 **/
@Component
public class AreaDataJob {

    private static final Logger logger = LoggerFactory.getLogger(AreaDataJob.class);

    @Autowired
    private AreaDataService areaDataService;

//    @Scheduled(cron = "0 1 10 * * ?")
//    private void queryAreaData(){
//
//        try {
//            AreaDataRequest areaDataRequest = new AreaDataRequest();
//            areaDataRequest.setArea(DateTimeUtil.getNow("yyyy-MM-dd"));
//            areaDataService.queryAreaData(areaDataRequest);
//        }catch (Exception e) {
//            logger.error("<<<<<< queryAreaData  process
//
//            error cause", e);
//        }
//
//    }

    @Scheduled(cron = "0 0 10 * * ?")
    private void getAreaData(){

        try {
            areaDataService.getAreaData();
        }catch (Exception e) {
            logger.error("<<<<<< getAreaData  process  error cause", e);
        }

    }

}
