package com.study.job;

import com.study.impl.BasicDataAddServiceImpl;
import com.study.service.BasicDataAddService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @Author: liyuankun
 * @Date: 2021/1/28 17:18
 * @Description: 基础数据新增定时任务
 **/
@Component
public class BasicDataAddJob {

    private static final Logger logger = LoggerFactory.getLogger(BasicDataAddJob.class);

    @Autowired
    private BasicDataAddService basicDataAddService;

    @Scheduled(cron = "0 0/30 10-22 * * ?")
    private void process(){

        try {
            basicDataAddService.getBasicDataAdd();
        }catch (Exception e) {



            logger.error("<<<<<< BasicDataAddJob  process  error cause", e);
        }


    }


}
